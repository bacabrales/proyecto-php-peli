<?php

include_once "encabezado.php";

?>
<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Ingrese a su cuenta</h2>
						<form action="#">
                            <input type="email" placeholder="Correo electronico" />
                            <input type="password" placeholder="Contraseña" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Recordar correo
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">O</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>¡Nuevo registro de usuario!</h2>
						<form action="#">
							<input type="text" placeholder="Nombres y Apellidos"/>
							<input type="email" placeholder="Correo electronico"/>
							<input type="password" placeholder="Contraseña"/>
							<button type="submit" class="btn btn-default">Registrar</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
    </section><!--/form-->
    
<?php 
include_once "footer.php";
?>